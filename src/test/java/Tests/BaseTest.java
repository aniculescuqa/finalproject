package Tests;

import Data.TestData;
import DriverFactory.DriverFactory;
import Utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;

public class BaseTest {
    private WebDriver driver;
    public TestData testData;

    @BeforeMethod
    public void setup() {
        try {
            testData = new TestData();
            this.driver = DriverFactory.getDriver(testData.getBrowser());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterMethod
    public void tearDownOrScreenShot(ITestResult result) {
        //using ITestResult.FAILURE is equals to result.getStatus then it enter into if condition
        if (ITestResult.FAILURE == result.getStatus() && ((RemoteWebDriver) driver).getSessionId() != null)
            Utils.takeScreenShot(driver, result.getName());
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

}
