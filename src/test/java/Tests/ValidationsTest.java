package Tests;

import Pages.ValidationsPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ValidationsTest extends BaseTest {

    @DataProvider(name = "negativeLoginData")
    public Iterator<Object[]> negativeLogin() {
        Collection<Object[]> values = new ArrayList<Object[]>();
        // emailvalue, passwordvalue, email error, password error, general error
        values.add(new String[]{"gda2rdsf@yafsdfhoo.com", "aaaaa", "", "", "Adresa de email nu exista in baza noastra de date. Va rugam sa incercati din nou."});
        values.add(new String[]{"1", "1", "Va rugam introduceti o adresa de email valida", "Parola trebuie sa contina minim 3 caractere", ""});
        values.add(new String[]{"", "", "Va rugam completati adresa de email", "Va rugam completati parola", ""});
        return values.iterator();
    }

    @Test(dataProvider = "negativeLoginData")
    public void loginNegativeTest(String username, String password, String userError, String passError, String generalError) {
        ValidationsPage validationsPage = new ValidationsPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl() + "autentificare/");
        validationsPage.fillInLogin(username, password);
        if (userError.length() > 0) {
            Assert.assertTrue(validationsPage.validateLoginError(userError, "userError"));
            System.out.println("UserName Error was verified: " + userError);
        }
        if (passError.length() > 0) {
            Assert.assertTrue(validationsPage.validateLoginError(passError, "passError"));
            System.out.println("PassWord Error was verified: " + passError);
        }
        if (generalError.length() > 0) {
            Assert.assertTrue(validationsPage.validateLoginError(generalError, "generalError"));
            System.out.println("General Error was verified: " + generalError);
        }
    }

    @DataProvider(name = "negativeRegisterData")
    public Iterator<Object[]> negativeRegister() {
        Collection<Object[]> values = new ArrayList<Object[]>();

        values.add(new String[]{"", "", "", "", "", "",
                "Va rugam sa completati prenumele dvs.", "Va rugam sa completati numele dvs.",
                "Va rugam sa completati numarul dvs. de telefon", "Va rugam completati adresa de email",
                "Va rugam completati parola", "Va rugam sa confirmati parola", "*Campul acesta este obligatoriu"});
        values.add(new String[]{"", "", "1", "a", "1", "2345",
                "Va rugam sa completati prenumele dvs.", "Va rugam sa completati numele dvs.",
                "Numarul de telefon nu este valid", "Va rugam introduceti o adresa de email valida",
                "Parola trebuie sa contina minim 3 caractere", "Parolele nu corespund", "*Campul acesta este obligatoriu"});
        values.add(new String[]{"", "", "1", "a", "12", "1",
                "Va rugam sa completati prenumele dvs.", "Va rugam sa completati numele dvs.",
                "Numarul de telefon nu este valid", "Va rugam introduceti o adresa de email valida",
                "Parola trebuie sa contina minim 3 caractere", "Parola trebuie sa contina minim 3 caractere", "*Campul acesta este obligatoriu"});
        return values.iterator();
    }

    @Test(dataProvider = "negativeRegisterData")
    public void registerNegativeTest(String firstName, String lastName, String phone, String email,
                                     String password, String retypePassword, String firstNameError,
                                     String lastNameError, String phoneError, String emailError,
                                     String passwordError, String retypePasswordError, String checkBoxError) {

        ValidationsPage validationsPage = new ValidationsPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl() + "autentificare/");
        validationsPage.fillInRegister(firstName, lastName, phone, email, password, retypePassword);

        if (firstNameError.length() > 0) {
            Assert.assertTrue(validationsPage.validateRegisterError(firstNameError, "firstNameError"));
            System.out.println("firstName Error was verified: " + firstNameError);
        }
        if (lastNameError.length() > 0) {
            Assert.assertTrue(validationsPage.validateRegisterError(lastNameError, "lastNameError"));
            System.out.println("lastName Error was verified: " + lastNameError);
        }
        if (phoneError.length() > 0) {
            Assert.assertTrue(validationsPage.validateRegisterError(phoneError, "phoneError"));
            System.out.println("Phone Error was verified: " + phoneError);
        }
        if (emailError.length() > 0) {
            Assert.assertTrue(validationsPage.validateRegisterError(emailError, "emailError"));
            System.out.println("Email Error was verified: " + emailError);
        }
        if (passwordError.length() > 0) {
            Assert.assertTrue(validationsPage.validateRegisterError(passwordError, "passwordError"));
            System.out.println("Password Error was verified: " + passwordError);
        }
        if (retypePasswordError.length() > 0) {
            Assert.assertTrue(validationsPage.validateRegisterError(retypePasswordError, "retypePasswordError"));
            System.out.println("RetypePassword Error was verified: " + retypePasswordError);
        }
        if (checkBoxError.length() > 0) {
            Assert.assertTrue(validationsPage.validateRegisterError(checkBoxError, "checkBoxError"));
            System.out.println("CheckBox Error was verified: " + checkBoxError);
        }
    }
}

