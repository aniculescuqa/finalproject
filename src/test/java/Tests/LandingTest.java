package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LandingTest extends BaseTest {

    @Test(priority = 1)
    public void accessLandingPage() {
        Pages.LandingPage landingPage = new Pages.LandingPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        Assert.assertTrue(getDriver().getTitle().contains("PC Garage | Notebook"));
        System.out.println("Accessed Landing Page: " + getDriver().getTitle());
    }

    @Test(priority = 2)
    public void verifyLandingPageElements() {
        Pages.LandingPage landingPage = new Pages.LandingPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        Assert.assertTrue(landingPage.getLogo().contains("PC Garage - PC Garage | Notebook," +
                " calculatoare, sisteme, periferice si componente PC"));
        Assert.assertEquals(landingPage.getSearchButtonText(), "Cauta");
        Assert.assertTrue(landingPage.getMyCartText().contains("promotii"));
        Assert.assertTrue(landingPage.getFooterText().contains("PC Garage S.R.L., Nr. R.C.: J40/1207/2008, C.U.I.: RO17612390, Adresa: Str. Logofat Tautu 68A, Sector 3, Bucuresti"));
        System.out.println("Reached Landing page, and the Logo displays: " + landingPage.getLogo());
        System.out.println(landingPage.getSearchButtonText() + " text is present on search Button");
        System.out.println(landingPage.getMyCartText() + " text is present on myCart");
        System.out.println(landingPage.getFooterText() + " information is present in the Footer");
    }


    @Test(priority = 3)
    public void verifyNavigationBarElements() {
        Pages.LandingPage landingPage = new Pages.LandingPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        for (int i = 0; i < landingPage.getNavSize(); i++) {
            Assert.assertEquals(landingPage.getNaviList(i), testData.getNavBar(i), "NavBar `Assert Failed");
            System.out.println(landingPage.getNaviList(i) + " is present on the Navigation Bar");
        }
    }

    @Test(priority = 4)
    public void verifySearchArea() throws InterruptedException {
        Pages.LandingPage landingPage = new Pages.LandingPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        Thread.sleep(1000);
        landingPage.clickSearch();
        Assert.assertTrue(landingPage.getSearchFieldText().contains("Denumirea sau codul produsului"), "Search background is not present");
        Assert.assertTrue(landingPage.getSearchLabelText().contains("Categorie"), "Search category failed");
        System.out.println("Searchfield contains categories");
    }

    @Test(priority = 5)
    public void verifyPromotionsArea() {
        Pages.LandingPage landingPage = new Pages.LandingPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        landingPage.clickSearch();
        landingPage.clickMyCart();
        Assert.assertTrue(landingPage.getSort().contains("true"));
        Assert.assertTrue(landingPage.getSortType().contains("Top vanzari"));
        System.out.println(landingPage.getSortType() + " Sorting is selected by default");
    }
}
