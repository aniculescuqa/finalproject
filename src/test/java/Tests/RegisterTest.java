package Tests;

import Pages.RegisterPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static Utils.Utils.randomIntGenerator;
import static Utils.Utils.randomStringGenerator;

public class RegisterTest extends BaseTest {

    @DataProvider(name = "loginDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> register = new ArrayList<Object[]>();

        try {
            System.out.println("Connecting to Database...");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/siit_aut?useSSL=false&serverTimezone=UTC", "root", "Cstrike90?");
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("select * from registerdb;");
            System.out.println("Listing users:");
            while (results.next()) {
                register.add(new String[]{results.getString("firstname"), results.getString("lastname"),
                        results.getString("phone"), results.getString("email"), results.getString("password")});
                System.out.println(results.getString("firstname") + " " + results.getString("lastname") + " " + results.getString("email")
                        + " " + results.getString("phone") + " " + results.getString("password"));
            }
            results.close();
            connection.close();
        } catch (SQLException sexc) {
            System.out.println(sexc.getMessage());
            sexc.printStackTrace();
        }
        return register.iterator();
    }


    @Test(dataProvider = "loginDataProvider")
    public void loginWithDBUsers(String firstname, String lastname, String phone, String email, String password) throws InterruptedException {
        RegisterPage registerPage = new RegisterPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl() + "autentificare/");
        registerPage.fillInLogin(email, password);
        getDriver().navigate().to(testData.getBaseUrl() + "cont/date-personale/");
        Assert.assertTrue(registerPage.getRegisteredFirstName().contains(firstname));
        System.out.println("Database user's firstname was found: " + firstname);
        Assert.assertTrue(registerPage.getRegisteredLastName().contains(lastname));
        System.out.println("Database user's lastname was found: " + lastname);
        Assert.assertTrue(registerPage.getRegisteredTel().contains(phone));
        System.out.println("Database user's firstname was found: " + phone);
        System.out.println("Successfully logged in using DB Data and Verified");
    }

    @Test
    public void registerRandomUser() throws InterruptedException {
        String registerfName = randomStringGenerator(9);
        String registerLname = randomStringGenerator(9);
        String registerPhone = "0748333" + randomIntGenerator(111, 999);
        String registerEmail = randomStringGenerator(7) + "@" + randomIntGenerator(2, 6) + ".com";
        String registerPassword = randomStringGenerator(6) + randomIntGenerator(2, 4);

        RegisterPage registerPage = new RegisterPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl() + "autentificare/");
        registerPage.fillInRegister(registerfName, registerLname, registerPhone, registerEmail, registerPassword, registerPassword);
        registerPage.clickCheckBox();
        registerPage.clickRegister();
        System.out.println("Successfully Registered a random User");
        registerPage.clickDatePersonaleTab();
        System.out.println("Accessed the Data Tab");
        Assert.assertEquals(registerPage.getRegisteredFirstName(), registerfName);
        Assert.assertEquals(registerPage.getRegisteredLastName(), registerLname);
        Assert.assertEquals(registerPage.getRegisteredTel(), registerPhone);
        System.out.println("The random Registered user's data matches the generated data");
        System.out.println("The user's firstName is " + registerfName + ", lastname is " + registerLname +
                " and phone number is " + registerPhone);
    }
}



