package Tests;

import Pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @Test(priority = 1)
    public void accessLoginPage() {
        LoginPage loginPage = new LoginPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        loginPage.clickMyAccount();
        Assert.assertTrue(getDriver().getTitle().contains("Autentificare - PC Garage"), "Title does not contain the keyword");
        Assert.assertTrue(loginPage.geth1Text().contains("Autentificare"));
        System.out.println("Accessed Landing Page: " + getDriver().getTitle());
    }

    @Test(priority = 2)
    public void logIn() {
        LoginPage loginPage = new LoginPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        loginPage.clickMyAccount();
        loginPage.fillInEmail(testData.getUsername());
        loginPage.fillInPassword(testData.getPassword());
        loginPage.clickLogin();
        Assert.assertTrue(loginPage.getLoggedInH1().contains("Salut Andrei!"), "H1 Text wasn't there");
        Assert.assertTrue(loginPage.getLoginNameText().contains("Andrei Niculescu"), "Login Name assert Failed");
        System.out.println("You are now logged in as: " + loginPage.getLoginNameText());
    }

    @Test(priority = 3)
    public void checkHistoric() {
        LoginPage loginPage = new LoginPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        loginPage.clickMyAccount();
        loginPage.fillInEmail(testData.getUsername());
        loginPage.fillInPassword(testData.getPassword());
        loginPage.clickLogin();
        loginPage.clickHistory();
        Assert.assertTrue(loginPage.getLoggedInH1().contains("Istoric comenzi"));
        testData.setListOrder(loginPage.getOrderText());
        loginPage.clickOrder();
        Assert.assertTrue(loginPage.getLoggedInH1().contains(testData.getListOrder()));
        Assert.assertTrue(loginPage.getOrderStatusText().contains("efectuata"));
        System.out.println("The order list was accessed, " + "the following order was selected " +
                testData.getListOrder() + " and the order status is " + loginPage.getOrderStatusText());
    }

    @Test(priority = 4)
    public void addItemToCart() {
        LoginPage loginPage = new LoginPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        loginPage.clickMyAccount();
        loginPage.fillInEmail(testData.getUsername());
        loginPage.fillInPassword(testData.getPassword());
        loginPage.clickLogin();
        loginPage.clickHistory();
        testData.setListOrder(loginPage.getOrderText());
        loginPage.clickOrder();
        loginPage.fillInSearch(testData.getSearch3());
        loginPage.clickSearch();
        System.out.println("Searching for: " + testData.getSearch3());

        for (int i = 0; i < loginPage.getResultSize(); i++) {
            if (loginPage.getResult(i).equals("Tastatura Gaming Logitech G213 Prodigy Gaming")) {
                try {
                    System.out.println("Added requested item to cart: " + loginPage.getResult(i));
                    loginPage.clickAddItem(i);
                } catch (Exception e) {
                }
            }
        }
        Assert.assertTrue(loginPage.getCartText().contains("Salveaza continutul cosului ca wishlist"));
        System.out.println("You are now on the cart page");
        if (loginPage.getItemNumber() > 1) {
            loginPage.fillInItemNumber("1");
            loginPage.clickUpdate();
        }
        loginPage.removeItems();
        Assert.assertTrue(loginPage.getEmptyCartText().contains("Cosul tau de cumparaturi este gol"));
        System.out.printf("Removed all the items from cart");
    }

    @Test(priority = 5)
    public void logout() {
        LoginPage loginPage = new LoginPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl());
        loginPage.clickMyAccount();
        loginPage.fillInEmail(testData.getUsername());
        loginPage.fillInPassword(testData.getPassword());
        loginPage.clickLogin();
        loginPage.clickHistory();
        System.out.println("Making sure the coookie named pcgarage exists and has a value");
        Assert.assertTrue(getDriver().manage().getCookieNamed("pcgarage").getValue() != null);
        loginPage.clickLogout();
        Assert.assertTrue(loginPage.getLogoutButtonText().contains("Ai iesit din contul tau"));
        getDriver().manage().deleteAllCookies();
        Assert.assertTrue(getDriver().manage().getCookieNamed("pcgarage") == null);
        System.out.println("Making sure the coookie named pcgarage was deleted after logging out and deleting cookies");
    }
}
