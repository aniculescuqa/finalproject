package Tests;

import Pages.SearchPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchTest extends BaseTest {

    @Test(priority = 1)
    public void accessSearchPage() {
        SearchPage searchPage = new SearchPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl() + "cauta");
        Assert.assertTrue(getDriver().getTitle().contains("PC Garage"), "Title does not contain");
        System.out.println("Accessed Landing Page: " + getDriver().getTitle());
    }

    @Test(priority = 2)
    public void searchForItem() {
        SearchPage searchPage = new SearchPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl() + "cauta");
        searchPage.fillInSearchField(testData.getSearch1());
        searchPage.clickSearchButton();
        Assert.assertTrue(searchPage.getSearchResultsText().contains(testData.getSearch1()));
        System.out.println("Successfully searched for item, and found  " + searchPage.getSearchResultsText());
    }

    @Test(priority = 3)
    public void validateSearchedItemResults() {
        SearchPage searchPage = new SearchPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl() + "cauta");
        searchPage.fillInSearchField(testData.getSearch1());
        searchPage.clickSearchButton();
        System.out.println("Every displayed result has " + testData.getSearch1() + " in the text");

        for (int i = 0; i < searchPage.getResultListSize(); i++) {
            Assert.assertTrue(searchPage.getSearchResultsList(i).contains(testData.getSearch1()),
                    "An item does not have the searched text in the title");
            System.out.println(searchPage.getSearchResultsList(i));
        }
    }

    @Test(priority = 4)
    public void invalidSearch() {
        SearchPage searchPage = new SearchPage(getDriver());
        getDriver().navigate().to(testData.getBaseUrl() + "cauta");
        searchPage.fillInSearchField("nampleaca");
        searchPage.clickSearchButton();
        Assert.assertTrue(searchPage.getSearchResultsText().contains("0 rezultate"));
        Assert.assertTrue(searchPage.getSearchErrorText().contains
                ("Nu a fost gasit niciun produs corespunzator criteriilor de cautare."));
        System.out.println("No items were successfully found");
    }

    @Test(priority = 5)
    public void searchByKeywordAndCategory() {
        SearchPage searchPage = new SearchPage(getDriver());
        getDriver().get("https://pcgarage.ro/cauta");
        searchPage.fillinKeyWord(testData.getSearch2());
        searchPage.selectCategoryField();
        searchPage.selectCategory(3);
        searchPage.clickLargeSearchButton();
        System.out.println("All the items found contain the searched keyword in the title: ");

        for (int i = 0; i < searchPage.getResultListSize(); i++) {
            Assert.assertTrue(searchPage.getSearchResultsList(i).contains(testData.getSearch2()),
                    "An item does not have the searched text in the title");
            System.out.println(searchPage.getSearchResultsList(i));
        }
    }

}
