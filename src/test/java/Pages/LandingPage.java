package Pages;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class LandingPage extends BasePage {

    @FindBy(id = "logo")
    private WebElement pcgLogoLocator;

    @FindBy(className = "cat-nav-tab")
    private List<WebElement> naviBarlocator;

    @FindBy(id = "sbbt")
    private WebElement searchButtonLocator;

    @FindBy(id = "searchac")
    private WebElement searchFieldLocator;

    @FindBy(id = "cart-header")
    private WebElement myCartLocator;

    @FindBy(css = "div.footer-disclaimer > p:nth-child(2)")
    private WebElement footerTextLocator;

    @FindBy(id = "search_cats")
    private WebElement searchContextLabelLocator;

    @FindBy(xpath = "//*[@id=\"search_products\"]/div[1]/label/text()")
    private WebElement searchContextCategoryLocator;

    @FindBy(xpath = "//*[@id=\"search_products\"]/div[2]/label/text()")
    private WebElement searchContextCategory2Locator;

    @FindBy(css = "#sortsel > option:nth-child(1)")
    private WebElement sortLocator;

    public LandingPage(WebDriver driver) {
        super(driver);
    }

    public String getLogo() {
        waitForElementToBeVisible(pcgLogoLocator);
        return pcgLogoLocator.getText();
    }

    public void clickSearch() {
        waitForElementToBeVisible(searchFieldLocator);
        searchFieldLocator.click();
    }

    public String getSearchButtonText() {
        waitForElementToBeVisible(searchButtonLocator);
        return searchButtonLocator.getText();
    }

    public String getSearchFieldText() {
        waitForElementToBeVisible(searchFieldLocator);
        return searchFieldLocator.getAttribute("placeholder");
    }

    public String getNaviList(int index) {

        return naviBarlocator.get(index).getText();
    }

    public int getNavSize() {
        return naviBarlocator.size();
    }

    public String getFooterText() {
        scrollToElementAndWait(footerTextLocator);
        return footerTextLocator.getText();
    }

    public String getMyCartText() {
        waitForElementToBeVisible(myCartLocator);
        return myCartLocator.getText();
    }

    public String getSearchLabelText() {
        waitForElementToBeVisible(searchContextLabelLocator);
        return searchContextLabelLocator.getText();
    }

    public String getSort() {
        waitForElementToBeVisible(sortLocator);
        return sortLocator.getAttribute("selected");
    }

    public String getSortType() {
        waitForElementToBeVisible(sortLocator);
        return sortLocator.getText();
    }

    public void clickMyCart() {
        waitAndClick(myCartLocator);
    }
}
