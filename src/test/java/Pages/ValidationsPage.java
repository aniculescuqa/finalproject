package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ValidationsPage extends BasePage {

    @FindBy(id = "email")
    private WebElement emailFieldLocator;

    @FindBy(id = "password")
    private WebElement passwordFieldLocator;

    @FindBy(xpath = "//button[text()[contains(.,'Autentificare')]]")
    private WebElement loginButtonLocator;

    @FindBy(id = "newfirstname")
    private WebElement registerFirstNameLocator;

    @FindBy(id = "newlastname")
    private WebElement registerLastNameLocator;

    @FindBy(id = "telephone")
    private WebElement registerTelLocator;

    @FindBy(id = "newemail")
    private WebElement registerEmailLocator;

    @FindBy(id = "newpassword")
    private WebElement registerPasswordLocator;

    @FindBy(id = "newpasswordretype")
    private WebElement registerRetypePasswordLocator;

    @FindBy(xpath = "//button[text()='Creeaza cont']")
    private WebElement registerButtonLocator;

    // Error Texts
    @FindAll(@FindBy(css = "label#email-error"))
    private List<WebElement> emailErrorTextLocator;

    @FindAll(@FindBy(id = "password-error"))
    private List<WebElement> passwordErrorTextLocator;

    @FindAll(@FindBy(css = "#fixed-content-wrapper > p"))
    private List<WebElement> generalLoginErrorTextLocator;

    @FindAll(@FindBy(id = "newfirstname-error"))
    private List<WebElement> registerfirstNameErrorTextLocator;

    @FindAll(@FindBy(id = "newlastname-error"))
    private List<WebElement> registerlastNameErrorTextLocator;

    @FindAll(@FindBy(id = "telephone-error"))
    private List<WebElement> registerTelephoneErrorTextLocator;

    @FindAll(@FindBy(id = "newemail-error"))
    private List<WebElement> registerEmailErrorTextLocator;

    @FindAll(@FindBy(id = "newpassword-error"))
    private List<WebElement> registerPasswordErrorTextLocator;

    @FindAll(@FindBy(id = "newpasswordretype-error"))
    private List<WebElement> registerRetypePasswordErrorTextLocator;

    @FindAll(@FindBy(id = "confidentialitate-error"))
    private List<WebElement> checkBoxErrorTextLocator;


    public ValidationsPage(WebDriver driver) {
        super(driver);
    }

    public boolean validateLoginError(String error, String type) {
        if (type.equals("userError") && emailErrorTextLocator.size() != 0) {
            return error.equals(emailErrorTextLocator.get(0).getText());
        } else if (type.equals("passError") && passwordErrorTextLocator.size() != 0) {
            return error.equals(passwordErrorTextLocator.get(0).getText());
        } else if (type.equals("generalError") && generalLoginErrorTextLocator.size() != 0) {
            return error.equals(generalLoginErrorTextLocator.get(0).getText());
        }
        return false;
    }

    public boolean validateRegisterError(String error, String type) {
        if (type.equals("firstNameError") && registerfirstNameErrorTextLocator.size() != 0) {
            return error.equals(registerfirstNameErrorTextLocator.get(0).getText());
        } else if (type.equals("lastNameError") && registerlastNameErrorTextLocator.size() != 0) {
            return error.equals(registerlastNameErrorTextLocator.get(0).getText());
        } else if (type.equals("phoneError") && registerTelephoneErrorTextLocator.size() != 0) {
            return error.equals(registerTelephoneErrorTextLocator.get(0).getText());
        } else if (type.equals("emailError") && registerEmailErrorTextLocator.size() != 0) {
            return error.equals(registerEmailErrorTextLocator.get(0).getText());
        } else if (type.equals("passwordError") && registerPasswordErrorTextLocator.size() != 0) {
            return error.equals(registerPasswordErrorTextLocator.get(0).getText());
        } else if (type.equals("retypePasswordError") && registerRetypePasswordErrorTextLocator.size() != 0) {
            return error.equals(registerRetypePasswordErrorTextLocator.get(0).getText());
        } else if (type.equals("checkBoxError") && checkBoxErrorTextLocator.size() != 0) {
            return error.equals(checkBoxErrorTextLocator.get(0).getText());
        }
        return false;
    }

    public void fillInLogin(String username, String password) {
        waitAndFill(emailFieldLocator, username);
        waitAndFill(passwordFieldLocator, password);
        waitAndClick(loginButtonLocator);
    }

    public void fillInRegister(String firstName, String lastName, String phone, String email, String password, String retypePassword) {
        waitAndFill(registerFirstNameLocator, firstName);
        waitAndFill(registerLastNameLocator, lastName);
        waitAndFill(registerTelLocator, phone);
        waitAndFill(registerEmailLocator, email);
        waitAndFill(registerPasswordLocator, password);
        waitAndFill(registerRetypePasswordLocator, retypePassword);
        jsClick(registerButtonLocator);
    }

}
