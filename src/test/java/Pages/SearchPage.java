package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchPage extends BasePage {

    @FindBy(id = "sbbt")
    private WebElement searchButtonLocator;

    @FindBy(id = "searchac")
    private WebElement searchFieldLocator;

    @FindBy(css = "#container > div.main-content.clearfix > h1")
    private WebElement searchResultsTextLocator;

    @FindBy(css = "div.pb-name > a")
    private List<WebElement> resultsListLocator;

    @FindBy(className = "error-text")
    private WebElement errorTextLocator;

    @FindBy(css = "input#keyword")
    private WebElement keywordFieldLocator;

    @FindBy(css = ".active-result")
    private List<WebElement> categoryList;

    @FindBy(css = "#category_chosen > ul > li > input")
    private WebElement categoryFieldLocator;

    @FindBy(className = "large-submit")
    private WebElement largeSearchButtonLocator;

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public String getSearchResultsList(int index) {
        return resultsListLocator.get(index).getText();
    }

    public int getResultListSize() {
        return resultsListLocator.size();
    }

    public void fillInSearchField(String value) {
        waitAndFill(searchFieldLocator, value);
    }

    public void fillinKeyWord(String value) {
        waitAndFill(searchFieldLocator, value);
    }

    public void clickSearchButton() {
        waitAndClick(searchButtonLocator);
        refreshPageWhileError();
    }

    public String getSearchResultsText() {
        return getElementText(searchResultsTextLocator);
    }

    public String getSearchErrorText() {
        return getElementText(errorTextLocator);
    }

    public void selectCategoryField() {
        waitAndClick(categoryFieldLocator);
    }

    public void selectCategory(int index) {
        waitAndClick(categoryList.get(index));
    }

    public void clickLargeSearchButton() {
        waitAndClick(largeSearchButtonLocator);
    }

}
