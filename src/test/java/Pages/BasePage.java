package Pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static java.lang.Thread.sleep;


public class BasePage {
    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Object evaluateJavascript(String script, Object... params) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return js.executeScript(script, params);
    }

    public FluentWait<WebDriver> getWait() {
        return new WebDriverWait(driver, 5L)
                .pollingEvery(Duration.ofMillis(200L))
                .ignoring(NoSuchElementException.class)
                .ignoring(NoSuchFrameException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementNotInteractableException.class)
                .ignoring(InvalidElementStateException.class);
    }

    public void waitXseconds(long seconds) {
        try {
            sleep(Integer.parseInt(seconds + "000"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void jsClick(WebElement element) {
        evaluateJavascript("arguments[0].click();", element);
    }

    public void jsFill(WebElement element, String value) {
        evaluateJavascript("arguments[0].value='" + value + "';", element);
    }

    public void waitForElementToBeVisible(WebElement element) {
        getWait().until(ExpectedConditions.visibilityOf(element));
    }

    public void waitAndClick(WebElement element) {
        getWait().until(ExpectedConditions.visibilityOf(element));
        element.click();
    }

    public void waitAndFill(WebElement element, String value) {
        getWait().until(ExpectedConditions.visibilityOf(element));
        element.clear();
        element.sendKeys(value);
    }

    public void scrollToElementAndWait(WebElement element) {
        waitForElementToBeVisible(element);
        evaluateJavascript("arguments[0].scrollIntoView(true);", element);
    }

    public String getElementText(WebElement element) {
        waitForElementToBeVisible(element);
        return element.getText();
    }

    public void refreshPageWhileError() {
        while (driver.getPageSource().contains("Va rugam reincercati")) {
            driver.navigate().refresh();
            waitXseconds(2);
        }
    }
}