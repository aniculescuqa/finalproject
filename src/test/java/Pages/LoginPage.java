package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LoginPage extends BasePage {

    @FindBy(id = "user_header")
    private WebElement contulMeuButtonLocator;

    @FindBy(id = "email")
    private WebElement emailFieldLocator;

    @FindBy(id = "password")
    private WebElement passwordFieldLocator;

    @FindBy(xpath = "//button[text()[contains(.,'Autentificare')]]")
    private WebElement loginButtonLocator;

    @FindBy(css = "#user_header > nav > a:nth-child(2)")
    private WebElement logOutButtonLocator;

    @FindBy(css = "#user_header > nav > a:nth-child(1)")
    private WebElement loginNameLocator;

    @FindBy(css = "#listing-right > h1")
    private WebElement loggedInh1;

    @FindBy(css = "#left-column > div.lc-box.toggleable-menu > div:nth-child(2) > ul > li:nth-child(1) > a")
    private WebElement historyButtonLocator;

    @FindBy(css = "#fixed-content-wrapper > h1")
    private WebElement h1TextLocator;

    @FindBy(css = "#listing-right > div > div > div.df-form-padding-table > div:nth-child(2) > div:nth-child(2) > a")
    private WebElement orderLocator;

    @FindBy(css = "#listing-right > div > div:nth-child(1) > div > div > div:nth-child(2) > div:nth-child(4)")
    private WebElement orderStatusLocator;

    @FindBy(id = "sbbt")
    private WebElement searchButtonLocator;

    @FindBy(id = "searchac")
    private WebElement searchFieldLocator;

    @FindBy(css = "div.pb-name > a")
    private List<WebElement> loginResultsListLocator;

    @FindBy(css = "div.pb-addtocart > a")
    private List<WebElement> addToCartListLocator;
    @FindBy(id = "dp_1411142")
    private WebElement itemNumberLocator;

    @FindBy(css = "#cart_form > table > tbody > tr:nth-child(2) > td.ct-quantity > a")
    private WebElement actualizeazaButtonLocator;

    @FindBy(css = "#cart_form > div.cart-extra.nobg > p:nth-child(1) > a > b")
    private WebElement removeItemsLocator;

    @FindBy(css = "#cart-page-desktop > div > p:nth-child(1) > b")
    private WebElement emptyCartTextLocator;

    @FindBy(css = "#cart_form > div.cart-extra.nobg > p.float-left > a > b")
    private WebElement cartTextLocator;

    @FindBy(css = "#logoutd > h1 > span")
    private WebElement logoutTextLocator;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void clickMyAccount() {
        waitAndClick(contulMeuButtonLocator);
    }

    public void clickLogin() {
        waitAndClick(loginButtonLocator);
    }

    public void fillInEmail(String value) {
        waitAndFill(emailFieldLocator, value);
    }

    public void fillInPassword(String value) {
        waitAndFill(passwordFieldLocator, value);
    }

    public String geth1Text() {
        return getElementText(h1TextLocator);
    }

    public String getLoggedInH1() {
        return getElementText(loggedInh1);
    }

    public String getLogoutButtonText() {
        return getElementText(logoutTextLocator);
    }

    public void clickLogout() {
        waitAndClick(logOutButtonLocator);
    }

    public String getLoginNameText() {
        return getElementText(loginNameLocator);
    }

    public void clickHistory() {
        waitAndClick(historyButtonLocator);
    }

    public void clickOrder() {
        waitAndClick(orderLocator);
    }

    public String getOrderText() {
        return getElementText(orderLocator);
    }

    public String getOrderStatusText() {
        return getElementText(orderStatusLocator);
    }

    public void fillInSearch(String value) {
        waitAndFill(searchFieldLocator, value);
    }

    public void clickSearch() {
        waitAndClick(searchButtonLocator);
    }

    public int getResultSize() {
        return loginResultsListLocator.size();
    }

    public String getResult(int index) {
        return loginResultsListLocator.get(index).getText();
    }

    public void clickAddItem(int index) {
        scrollToElementAndWait(addToCartListLocator.get(index));
        waitAndClick(addToCartListLocator.get(index));
    }

    public void fillInItemNumber(String number) {
        waitAndFill(itemNumberLocator, number);
    }

    public int getItemNumber() {
        return Integer.parseInt(itemNumberLocator.getAttribute("value"));
    }

    public void clickUpdate() {
        waitAndClick(actualizeazaButtonLocator);
    }

    public void removeItems() {
        waitAndClick(removeItemsLocator);
    }

    public String getEmptyCartText() {
        return getElementText(emptyCartTextLocator);
    }

    public String getCartText() {
        return getElementText(cartTextLocator);
    }

    public String getLogoutTexT() {
        return getElementText(logoutTextLocator);
    }
}

