package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage extends BasePage {

    @FindBy(css = "#fillInLogin > div > div > button")
    private WebElement submitRegisterButton;

    @FindBy(css = "#left-column > div.lc-box.toggleable-menu > div:nth-child(1) > ul > li:nth-child(2) > a")
    private WebElement datePersonaleTab;

    @FindBy(id = "newfirstname")
    private WebElement registerFirstNameLocator;

    @FindBy(id = "newlastname")
    private WebElement registerLastNameLocator;

    @FindBy(id = "telephone")
    private WebElement registerTelLocator;

    @FindBy(id = "newemail")
    private WebElement registerEmailLocator;

    @FindBy(id = "newpassword")
    private WebElement registerPasswordLocator;

    @FindBy(id = "newpasswordretype")
    private WebElement registerRetypePasswordLocator;

    @FindBy(xpath = "//button[text()='Creeaza cont']")
    private WebElement registerButtonLocator;

    @FindBy(id = "confidentialitate")
    private WebElement checkBox;

    @FindBy(id = "firstname")
    private WebElement firstNameLoginLocator;

    @FindBy(id = "lastname")
    private WebElement lastNameLoginLocator;

    @FindBy(id = "telephone1")
    private WebElement telephoneLoginLocator;

    @FindBy(id = "email")
    private WebElement emailFieldLocator;

    @FindBy(id = "password")
    private WebElement passwordFieldLocator;

    @FindBy(xpath = "//button[text()[contains(.,'Autentificare')]]")
    private WebElement loginButtonLocator;


    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public String getRegisteredFirstName() {
        waitForElementToBeVisible(firstNameLoginLocator);
        return firstNameLoginLocator.getAttribute("value");
    }

    public String getRegisteredLastName() {
        waitForElementToBeVisible(lastNameLoginLocator);
        return lastNameLoginLocator.getAttribute("value");
    }

    public String getRegisteredTel() {
        waitForElementToBeVisible(telephoneLoginLocator);
        return telephoneLoginLocator.getAttribute("value");
    }

    public void clickCheckBox() throws InterruptedException {
        Thread.sleep(1000);
        jsClick(checkBox);
    }

    public void fillInLogin(String username, String password) {
        waitAndFill(emailFieldLocator, username);
        waitAndFill(passwordFieldLocator, password);
        waitAndClick(loginButtonLocator);
    }

    public void fillInRegister(String firstName, String lastName, String phone, String email, String password, String retypePassword) {
        waitAndFill(registerFirstNameLocator, firstName);
        waitAndFill(registerLastNameLocator, lastName);
        waitAndFill(registerTelLocator, phone);
        waitAndFill(registerEmailLocator, email);
        waitAndFill(registerPasswordLocator, password);
        waitAndFill(registerRetypePasswordLocator, retypePassword);
        jsClick(registerButtonLocator);
    }

    public void clickRegister() {
        jsClick(registerButtonLocator);
    }

    public void clickDatePersonaleTab() {
        jsClick(datePersonaleTab);
    }
}
