package Utils;

import com.gargoylesoftware.htmlunit.javascript.host.intl.DateTimeFormat;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class Utils {
    public static int randomIntGenerator(int low, int high) {
        SecureRandom random = new SecureRandom();
        return random.nextInt(high - low + 1) + low;
    }

    public static String randomStringGenerator(int stringLength) {
        return RandomStringUtils.randomAlphabetic(stringLength);
    }

    public static void takeScreenShot(WebDriver driver, String testName) {

        DateFormat dateFormat = new SimpleDateFormat("HHmmss_yyyyMMdd");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        String name = "FAILED_" + testName + "_" + LocalDateTime.now().getDayOfMonth() + " "
                + LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute() + ":"
                + LocalDateTime.now().getSecond() + ".png";
        String resultsPath = System.getProperty("user.dir") + "/test-output/" + name;

        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File(resultsPath);
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}