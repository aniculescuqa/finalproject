package Data;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestData {

    private String baseUrl;
    private String username;
    private String password;
    private String baseURI;
    private String browser;
    private String search3 = "prodigy";
    private String listOrder;
    private String search1 = "RTX 2080";
    private String search2 = "asus";
    private String navBar[] = {"Laptop", "Mobile", "Sisteme Gaming & Office", "Componente", "Gaming",
            "Televizoare", "Periferice & Monitoare", "Software", "Printing", "Retelistica & UPS",
            "Audio/Video", "Casa & ingrijire", "Home Gadgets", "Sport & Sanatate", "Auto & Calatorii", "Servicii"};

    public String getListOrder() {
        return listOrder;
    }

    public void setListOrder(String listOrder) {
        this.listOrder = listOrder;
    }

    public String getSearch3() {
        return search3;
    }

    public String getSearch2() {
        return search2.toUpperCase();
    }

    public String getSearch1() {
        return search1;
    }

    public String getNavBar(int index) {
        return navBar[index];
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getBrowser() {
        return browser;
    }

    public TestData() throws IOException {
        InputStream input = new FileInputStream("src/test/java/application.properties");
        Properties prop = new Properties();
        prop.load(input);
        baseUrl = prop.getProperty("baseUrl");
        username = prop.getProperty("username");
        password = System.getenv(prop.getProperty("password"));
        baseURI = prop.getProperty("baseURI");
        browser = prop.getProperty("browser");
    }

}
